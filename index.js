const { searchMovies, getMovieDetails } = require('./services/yifi')
const { download } = require('./services/torrent')
const { getPathOfMovie } = require('./services/utils')
const express = require('express')
const app = express()
const PORT = 3000
const fs = require('fs')
let isDownloaded = false
let downloaded

app.listen(PORT, () => {
  console.log(`Application listening on port ${PORT}`)
})

app.get('/', async (_req, res) => {
  res.set('Access-Control-Allow-Origin', '*')
  res.status(200).send('<html><h1>Welcome</h1></html>')
})

app.get('/movies', async (req, res) => {
  res.set('Access-Control-Allow-Origin', '*')
  const movies = await searchMovies(req.query)
  res.status(200).send(movies)
})

app.get('/movies/:id', async (req, res) => {
  res.set('Access-Control-Allow-Origin', '*')
  const movieDetails = await getMovieDetails(req.params.id)
  res.status(200).send(movieDetails)
})

const startDownload = (res, hash) => {
  download(hash, response => {
    downloaded = response.downloaded
    console.log(`${downloaded}% downloaded`)
    if (response.isDownloaded) {
      isDownloaded = true
    }
  })
  res.status(206).send({ downloaded: 0 })
}

const checkDownload = (res, path) => {
  if (!isDownloaded) {
    console.log('Downloading')
    res.status(206).send({ downloaded: downloaded })
  } else {
    console.log('Download complete')
    res.status(200).send({ path: path, downloaded: 100 })
  }
}

app.get('/movies/:id/download/:hash', async (req, res) => {
  res.set('Access-Control-Allow-Origin', '*')

  const { hash } = req.params || {}
  const path = getPathOfMovie(hash)

  if (!fs.existsSync(path)) {
    console.log('Start download')
    startDownload(res, hash)
  } else {
    checkDownload(res, path)
  }
})
