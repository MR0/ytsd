# YTS Downloader

Movie download helper utility

## Features

- Searching for a movie in the YTS catalog
- Selection of the film from a list of films found
- Downloading a specific movie

## Prerequisite

- node

## Usage

### Install and lauch

```bash
npm install
npm start
```

### Use

3 endpoints are available :

| **endpoint**               | **Method** | **Description**       | **Parameter**                          |
| -------------------------- | ---------- | --------------------- | -------------------------------------- |
| /movies                    | GET        | Get movies            | query_term (optional)                  |
| /movies/:id                | GET        | Get movie information | id of the movie                        |
| /movies/:id/download/:hash | GET        | Download movie        | id of the movie<br>hash of the torrent |


By default, the port is define on **3000**

For example, to find all movies with the title _fury road_, use :

```bash
curl --location --request GET 'localhost:3000/movies?query_term=fury road'
```

## Sources

- [yts-api](https://yts.mx/api)
