const fs = require('fs')

const determineQueryParam = ({ page, limit, query_term }) => {
  const baseUrl = 'https://yts.mx/api/v2/list_movies.json'

  if (query_term) {
    return `${baseUrl}?query_term=${query_term}`
  }

  if (page) {
    if (limit) {
      return `${baseUrl}?page=${page}&limit=${limit}`
    } else {
      return `${baseUrl}?page=${page}`
    }
  } else {
    return baseUrl
  }
}

const getUsefulElements = movies => {
  const movieList = []
  for (let movie of movies) {
    const { id, title, medium_cover_image } = movie || {}
    if (
      id !== undefined &&
      title !== undefined &&
      medium_cover_image !== undefined
    ) {
      movieList.push({ id: id, title: title, coverImage: medium_cover_image })
    }
  }
  return movieList
}

const getUsefulDetailsElements = movieDetails => {
  const {
    id,
    imdb_code,
    title,
    title_english,
    year,
    rating,
    genres,
    description_intro,
    background_image,
    medium_cover_image,
    torrents,
  } = movieDetails || {}
  if (
    id !== undefined &&
    imdb_code !== undefined &&
    title !== undefined &&
    title_english !== undefined &&
    year !== undefined &&
    rating !== undefined &&
    genres !== undefined &&
    description_intro !== undefined &&
    background_image !== undefined &&
    medium_cover_image !== undefined &&
    torrents !== undefined
  ) {
    return {
      id: id,
      imdbCode: imdb_code,
      title: title,
      titleEnglish: title_english,
      year: year,
      rating: rating,
      genres: genres,
      description: description_intro,
      backgroundImage: background_image,
      coverImage: medium_cover_image,
      torrents: torrents,
    }
  } else {
    return {}
  }
}

// TODO - Faire du récursif ici ?
const getPathOfMovie = hash => {
  const basePath = `./movies/${hash}`

  try {
    const dir = fs.readdirSync(basePath)
    const movieDir = dir.find(d => d != 'torrent-stream')
    const d = fs.readdirSync(`${basePath}/${movieDir}`)
    const moviePath = `${basePath}/${movieDir}/${d.find(film =>
      film.includes('mp4')
    )}`
    return moviePath
  } catch (err) {
    return basePath
  }
}

module.exports = {
  determineQueryParam,
  getUsefulElements,
  getUsefulDetailsElements,
  getPathOfMovie,
}
