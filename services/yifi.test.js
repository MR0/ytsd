const { searchMovies, getMovieDetails } = require('./yifi.js')
const {
  determineQueryParam,
  getUsefulElements,
  getUsefulDetailsElements,
} = require('./utils.js')
const axios = require('axios')

jest.mock('axios')
jest.mock('./utils.js')

describe('searchMovies', () => {
  it('should return something even if there is no parameter', async () => {
    // GIVEN
    determineQueryParam.mockResolvedValue('')
    axios.get.mockResolvedValue({ data: { data: { movies: [] } } })
    getUsefulElements.mockResolvedValue([])

    // WHEN
    const result = await searchMovies({})

    // THEN
    expect(result).toStrictEqual([])
  })

  it('should return empty array when an error is catched', async () => {
    // GIVEN
    determineQueryParam.mockResolvedValue('')
    axios.get.mockRejectedValueOnce(new Error('Argh'))

    // WHEN
    const result = await searchMovies({})

    // THEN
    expect(result).toStrictEqual([])
  })
})

describe('get movie details', () => {
  it('should return null when there is not id in parameter', async () => {
    const result = await getMovieDetails()
    expect(result).toBe(null)
  })

  it('should return an object when there is an id in parameter', async () => {
    // GIVEN
    axios.get.mockResolvedValue({ data: { data: { movie: {} } } })
    getUsefulDetailsElements.mockResolvedValue({})

    // WHEN
    const result = await getMovieDetails(1)

    // THEN
    expect(result).toStrictEqual({})
  })
})
