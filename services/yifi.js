const axios = require('axios')

/**
 * Recherche d'un film depuis un titre
 * @param {string} title le titre du film recherché
 * @returns une liste de résultats
 */
const search = async (title) => {
  console.log(`Recherche de ${title}`)
  const results = []

  /* Appel de l'api pour récupérer les films recherchés */
  const res = await axios.get(
    `https://yts.mx/api/v2/list_movies.json?query_term=${title}&sort_by=rating`
  )

  if (res.status === 200) {
    /* Récupération des films trouvés */
    const { movies, movie_count } = res.data.data

    if (!movies) {
      console.log(`${title} introuvable`)
      process.exit()
    }

    console.log(
      movie_count > 1 ? `${movie_count} films trouvés` : '1 film trouvé'
    )
    let id = 1

    for (let movie of movies) {
      let max_peers = 0
      let hash
      /* Permet de décider quel sera le fichier le plus rapide à télécharger */
      for (let torrent of movie.torrents) {
        if (torrent.peers > max_peers) {
          max_peers = torrent.peers
          hash = torrent.hash
        }
      }
      /* Construction de la réponse */
      results.push({
        id: id,
        title: movie.title_long,
        rating: movie.rating,
        imdbCode: movie.imdb_code,
        hash: hash,
      })
      id++
    }

    return results
  } else {
    console.log('Un problème est survenu')
  }
}

const PAGE = 1 // page par défaut
const {
  determineQueryParam,
  getUsefulElements,
  getUsefulDetailsElements,
} = require('./utils.js')

const searchMovies = async ({ page, query_term }, limit) => {
  console.info('Récupération de la liste des films')
  if (!page) {
    page = PAGE
  }
  try {
    const { data } = await axios.get(
      determineQueryParam({ page, limit, query_term })
    )
    const { movies } = data.data
    return getUsefulElements(movies)
  } catch (err) {
    console.error('Impossible de récupérer les films', err)
    return []
  }
}

const getMovieDetails = async (id) => {
  if (id) {
    const { data } = await axios.get(
      `https://yts.mx/api/v2/movie_details.json?movie_id=${id}`
    )

    return getUsefulDetailsElements(data.data.movie)
  } else {
    return null
  }
}

module.exports = { search, searchMovies, getMovieDetails }
