const {
  determineQueryParam,
  getUsefulElements,
  getUsefulDetailsElements,
  getPathOfMovie,
} = require('./utils')

describe('determine query param', () => {
  it('should return empty query param', () => {
    expect(determineQueryParam({})).toBe(
      'https://yts.mx/api/v2/list_movies.json'
    )
  })

  it('should have an url with page 1', () => {
    expect(determineQueryParam({ page: 1 })).toBe(
      'https://yts.mx/api/v2/list_movies.json?page=1'
    )
  })

  it('should have an url with page 2', () => {
    expect(determineQueryParam({ page: 2 })).toBe(
      'https://yts.mx/api/v2/list_movies.json?page=2'
    )
  })

  it('should have 2 queries param : page and limit', () => {
    expect(determineQueryParam({ page: 1, limit: 1 })).toBe(
      'https://yts.mx/api/v2/list_movies.json?page=1&limit=1'
    )
  })

  it('should have query_term when only when query_term exists', () => {
    expect(
      determineQueryParam({ page: 1, limit: 1, query_term: 'title' })
    ).toBe('https://yts.mx/api/v2/list_movies.json?query_term=title')
  })
})

describe('get useful element', () => {
  it('should return empty array when there is nothing to get', () => {
    const badData = [{ index: 1, label: 'label' }]
    expect(getUsefulElements(badData)).toEqual([])
  })

  it('should return id, title and medium_cover_image only, when there is a lot to get', () => {
    const tooManyData = [
      {
        index: 1,
        label: 'label',
        id: 1,
        title: 'title',
        medium_cover_image: 'medium_cover_image',
      },
    ]
    expect(getUsefulElements(tooManyData)).toEqual([
      { id: 1, title: 'title', coverImage: 'medium_cover_image' },
    ])
  })
})

describe('get useful details elements', () => {
  it('should return empty object when there is nothing to get', () => {
    expect(getUsefulDetailsElements({})).toEqual({})
  })

  it('should return some properties only, when there is a lot to get', () => {
    const tooManyData = {
      index: 1,
      label: 'label',
      imdb_code: 'imdb_code',
      title: 'title',
      title_english: 'title_english',
      year: 'year',
      rating: 'rating',
      genres: [],
      description_intro: 'description_intro',
      background_image: 'background_image',
      medium_cover_image: 'medium_cover_image',
      torrents: [],
    }

    expect(getUsefulDetailsElements(tooManyData)).toEqual({
      imdbCode: 'imdb_code',
      title: 'title',
      titleEnglish: 'title_english',
      year: 'year',
      rating: 'rating',
      genres: [],
      description: 'description_intro',
      backgroundImage: 'background_image',
      coverImage: 'medium_cover_image',
      torrents: [],
    })
  })
})

describe('get size of downloaded movie', () => {
  it('should get the path of movie file', () => {
    // GIVEN
    const hash = 'ABCD1234'

    // WHEN/THEN
    expect(getPathOfMovie(hash)).toBe(
      './movies/ABCD1234/Test (2022) [1080p] [WEBRip] [5.1] [YTS.MX]/test.mp4'
    )
  })

  it('should get the path of movie file', () => {
    // GIVEN
    const hash = 'ABCD123'

    // WHEN/THEN
    expect(getPathOfMovie(hash)).toBe('./movies/ABCD123')
  })
})
