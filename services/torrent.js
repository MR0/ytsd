const torrentStream = require('torrent-stream')
const MOVIE_EXTENSIONS_REGEX = /(.*?)\.(avi|mp4|mkv|mov)$/i

const isMovie = file => {
  return MOVIE_EXTENSIONS_REGEX.test(file.name)
}

const updateDownloadedPercentage = (torrent, totalLength, callback) => {
  callback({
    downloaded: Math.floor((torrent.swarm.downloaded / totalLength) * 100),
  })
}

const start = (torrent, callback) => {
  torrent.files.forEach(file => {
    if (isMovie(file)) {
      console.log(`Download ${file.name}`)
      const movieStream = file.createReadStream()
      movieStream.on('data', () => {
        updateDownloadedPercentage(torrent, movieStream.length, callback)
      })
    }
  })
}

const stop = callback => {
  console.log('End of download')
  callback({
    isDownloaded: true,
    downloaded: 100,
  })
}

const download = (hash, callback) => {
  const path = `./movies/${hash}`
  const torrent = torrentStream(`magnet:?xt=urn:btih:${hash}`, {
    path: path,
    tmp: path,
  })

  torrent.on('ready', () => {
    start(torrent, callback)
  })

  torrent.on('idle', () => {
    stop(callback)
  })
}

module.exports = { download }
